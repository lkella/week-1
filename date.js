/**
 * A new planet was discovered. This is called Planet Alpha. 
Scientists have made a new calendar for this planet and it has only 9 months each and the number of days in each month is below.

January - 30
February - 20
March - 30
April - 25
May - 30
June - 25
July - 30
August - 30
September - 25

Date 02/03/04 means, in the year 04, the 2nd of March. Please note that 26/06/22 is an invalid date, because June has only 25 days. 

Write a function to calculate the difference between two date strings for this planet: "19/02/05" and "22/06/86"
 */






/**
 * checks for the validation of date
 * @param {string} str - date 
 * @returns {boolean}
 */

function validate_date_string(str) {
	if (str === null) {
		return false;       //if string is empty
	}
	s = str.split("/");     // checks the length of date
	if (s.length !== 3) {
		return false;
	}
	let dateformat = /^(0?[1-9]|[1-2][0-9]|30)[\/](0?[1-9])[\/]([0-9]*)$/; //date format
	if (dateformat.test(str)) {
		let s = str.split("/");
		day = parseInt(s[0]);    // day of date
		month = parseInt(s[1]);
		year = parseInt(s[2]);
		var months = [30, 20, 30, 25, 30, 25, 30, 30, 25];

		// checks for the day in month
		if (day <= months[month - 1]) {
			return true;

		} else {
			return false;
		}



	}

	// if string is not validated
	else {
		return false;
	}
}


/**
 * Represents which date is greater
 * @param {string} date1 - first date
 * @param {string} date2 - second date
 * @returns {1,0,-1}
 */

function greaterthan(date1, date2) {
	if (validate_date_string(date1) && validate_date_string(date2)) {
		var s1 = date1.split("/"); //20/09/20
		var s2 = date2.split("/"); //10/09/20
		day1 = parseInt(s1[0]); //day1
		month1 = parseInt(s1[1]); //month1
		year1 = parseInt(s1[2]); //year1
		day2 = parseInt(s2[0]); //day2
		month2 = parseInt(s2[1]); //month2
		year2 = parseInt(s2[2]); //year2

		// checking year
		if (year1 > year2) {
			return 1;
		}

		// if year is equal, checking for month
		else if (year1 === year2 && month1 > month2) {
			return 1;
		}

		// if year and month are equal, checking for day
		else if (year1 === year2 && month1 === month2) {
			if (day1 > day2) {
				return 1;
			} else if (day1 === day2) {
				return 0;
			}
		} else return -1;
	} else {
		return "Invalid date format";
	}
}

/**
 * Returns the next date
 * @param {string} str - date 
 * @returns date format
 */

function get_next(str) {
	let s = str.split("/");
	// s=['20','09','10']
	//converting into numbers
	day = parseInt(s[0]); //day
	month = parseInt(s[1]); //month
	year = parseInt(s[2]); //year

	// if day is in range of 1-20
	if (day < 20) {
		day += 1;
	}

	// if day = 20 and month = february
	else if (day === 20 && month === 2) {
		day = 1;
		month += 1;
	}

	// if day < 25
	else if (day < 25) {
		day += 1;
	}

	// if day = 25 and month = april , june
	else if (day === 25 && month in [4, 6]) {
		day = 1;
		month += 1;
	}

	// if day = 25 and month = september
	else if (month === 9) {
		day = month = 1;
		year += 1;
	}

	// if day < 29
	else if (day <= 29) day += 1;

	// if day = 30
	else if (day === 30) {
		day = 1;
		month += 1;
	}

	// if day < 10 returns the date format (dd/mm/yy)
	if (day < 10) {
		return "0" + String(day) + "/0" + String(month) + "/" + String(year);
	}
	return String(day) + "/0" + String(month) + "/" + String(year);
}


/**
 * It returns the total days in between the two dates
 * @param {string} str1 - first date
 * @param {string} str2 - second date
 * @returns count of days
 */


function get_difference(str1, str2) {
	if ((validate_date_string(str1) && validate_date_string(str2)) === false) {
		return "invalid date";
	}

	// if date 1 is greater than date 2 then swap
	if (greaterthan(str1, str2) === 1) {
		[str1, str2] = [str2, str1];

	}

	// if two dates are equal
	if (greaterthan(str1, str2) === 0) {
		return 0;
	}

	// iterates until both dates are equal
	count = 0;
	while (str1 !== str2) {
		count += 1;
		str1 = get_next(str1);
	}
	return count;
}
var x = get_difference("01/05/22","10/05/22");
console.log("Total number of days :" + x);


/**
 * Represents the week day of the date
 * @param {string} string - date
 * @returns 
 */
function get_day(string) {

	if (validate_date_string(string)) {
		var week_days = ['palyday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'funday', 'workday']
		month_days = [30, 20, 30, 25, 30, 25, 30, 30, 25]

		let s = string.split("/");
		var day = parseInt(s[0]);
		var month = parseInt(s[1]);
		var year = parseInt(s[2]);

		//total days
		var week_s = year * 245 + day;

		for (i = 0; i < month - 1; i++) {
			week_s = week_s + month_days[i]
		}

		week_count = week_s % 10;
		weekday_of_date = week_days[week_s]
		console.log(`Week day of the ${string}:` + weekday_of_date)
	}
	else {
		return "invalid"
	}
}


var x = get_day("01/01/00");
