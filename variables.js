// type of variables 

var numbers = 7; //integer
let stu_name = "lavanya"; //string
const grav_constant = 9.8; //decimal
const dept = ["ece", "cse"]; //list
let point; //undefined
let dict = { 1: "xyz", 2: "pqr" }; //dictionary
let example = [1, 2, 3, 4]; 
var bool = true;
var multiple = null;
let symbol = "_$"
var biginteger = 77412584458231478569n;

let type = (input) => {
    console.log("Type of " + input + " is " + typeof (input));
}

type(numbers);
type(stu_name);
type(grav_constant);
type(dept);
type(point);
type(dict);
type(example);
type(bool);
type(multiple);
type(symbol);
type(type);
type(biginteger)

