/**
 * To find the roots of quadratic expression
 * @param {*} a 
 * @param {*} b 
 * @param {*} c 
 * @returns roots
 */

function quadratic(a, b, c) {

	// discriminant  calculation
	var discriminant = Math.pow(b, 2) - 4 * a * c;

	//check if a ,b , c are equal to zero
	if (a == 0) {
		if (b == 0) {
			if (c == 0) {
				return 0;
			}
		} else {
			first_root = -c / b;
			return "roots:" + first_root;
		}
	}

	// checks the sign of discriminant
	else if (discriminant < 0) {
		//complex roots
		var realPart = (-b / (2 * a)).toFixed(2);
		var imagPart = (Math.sqrt(-discriminant) / (2 * a)).toFixed(2);

		first_root = realPart + " + " + imagPart + "i";
		second_root = realPart + " - " + imagPart + "i";
		return "roots:" + first_root + " , " + second_root;
	}
	else if (discriminant == 0) {
		first_root = -c / b;
		second_root = -b / 2 * a;
		return "roots:" + first_root + "," + second_root;
	}
	else {
		first_root = (-b - Math.sqrt(discriminant)) / 2 * a;
		second_root = (-b + Math.sqrt(discriminant)) / 2 * a;
		return "roots:" + first_root + "," + second_root;
	}

}



let x = quadratic(1, -2, 4);
console.log(x)